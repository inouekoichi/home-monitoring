#!/usr/bin/env python

import spidev
import datetime
import time
import csv
import codecs

class CustomFormat(csv.excel):
 quoting = csv.QUOTE_ALL


if __name__ == "__main__":

 spi=spidev.SpiDev()                               # genarate spi instance
 spi.open(0,0)                                        # select ADC/MCP3008 : bus=0, CE=0

 channel=0                                           # select CH0 : ADC/MCP3008

 buf = spi.xfer2([1,((8+channel)<<4),0])      # read adc data
 adResult = ((buf[1]&3)<<8)+buf[2]          # select data
 volt= adResult * 3.3 / 1023.0                # converte data to Voltage
 temp = (volt*1000.0 - 500.0)/10.0 # convertr volt to temp

 today = datetime.datetime.today()
 str_today = today.strftime("%Y/%m/%d %H:%M:%S")

 print(str_today)
 print(temp)

 spi.close()
 print ("ADC ic1 done")

 csvFile = codecs.open("/home/pi/Desktop/monitoring/temp.csv","a","utf-8")
 writer = csv.writer(csvFile,CustomFormat())

 row = (str_today,temp)
 writer.writerow(row)

 csvFile.close()
